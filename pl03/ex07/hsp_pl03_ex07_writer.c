#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>

#define ARRAY_SIZE 1000
#define MINIMUM 1
#define MAXIMUM 20

/* WRITER */

typedef struct {
	int array [ARRAY_SIZE];
} shm_data_type;

int main() {
	int fd, i, r;
	time_t t;
	shm_data_type *shared_mem;

	/* read,write,execute/search permissions by owner and group */
	fd = shm_open("/shmHSP", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);

	/* Verifica se a memória partilhada foi devidamente aberta */
	if (fd < 0) {
		perror("No smh_open!");
		exit(0);
	}

	/* Ajusta o tamanho da memória partilhada */
	ftruncate(fd, sizeof(shm_data_type));

	/* Mapea a memória partilhada */
	shared_mem = (shm_data_type *) mmap(NULL, sizeof(shm_data_type), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);

	/* Verifica se a memória partilhada foi devidamente mapeada */
	if (shared_mem == NULL) {
		printf("No mmap()");
		exit(0);
	}

	/* Inicializa o gerador aleatório */
	srand((unsigned) time(&t));

	/* Preenche o array com números aleatórios */
	for (i = 0; i < ARRAY_SIZE; i++) {
		shared_mem->array[i] = rand() % (MAXIMUM + 1 - MINIMUM) + MINIMUM;
		printf("\nNUMBER %d = %d", (i + 1), shared_mem->array[i]);
	}
	printf("\n\n");

	/* Desfaz o mapeamento */
	r = munmap(shared_mem, sizeof(shared_mem));

	if (r < 0) {
		perror("No munmap()!");
		exit(1);
	}

	return 0;
}
