#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <limits.h>

#define CHILDS 10
#define PATH_SIZE 250
#define WORD_SIZE 250

//BASED ON https://stackoverflow.com/questions/29429384/search-string-in-a-file-in-c

typedef struct{
	char path[PATH_SIZE];
	char word[WORD_SIZE];
	int times;
}info;

typedef struct {
	int index;
	info vec[CHILDS];
} shm_data_type;


int main() {
	int i,fd,r,y;


	shm_data_type *shared_mem;

	/* read,write,execute/search permissions by owner and group */
	fd = shm_open("/shmHSP", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);

	/* Verifica se a memória partilhada foi devidamente aberta */
	if (fd < 0) {
		perror("No smh_open!");
		exit(0);
	}

	/* Ajusta o tamanho da memória partilhada */
	ftruncate(fd, sizeof(shm_data_type));

	/* Mapea a memória partilhada */
	shared_mem = (shm_data_type *) mmap(NULL, sizeof(shm_data_type), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);

	/* Verifica se a memória partilhada foi devidamente mapeada */
	if (shared_mem == NULL) {
		printf("No mmap()");
		exit(0);
	}
	
	
	for(y=0;y<CHILDS;y++){
		printf("File: 1=test1 2=test2 3=test3\n");
		int path=0;	
		scanf("%d",&path);
		switch(path){
			case 1:
			strcpy(shared_mem->vec[y].path,"test1.txt");
			break;

			case 2:
			strcpy(shared_mem->vec[y].path,"test2.txt");
			break;

			case 3:
			strcpy(shared_mem->vec[y].path,"test3.txt");
			break;
		}
		
		printf("word:\n");
		char word[50];		
		scanf("%s",word);
		strcpy(shared_mem->vec[y].word,word);
		shared_mem->vec[y].times=0;
	
	}
	
		
	
	shared_mem->index=0;


	for(i=0;i<CHILDS;i++){
		if(fork()==0){
			FILE *in_file = fopen(shared_mem->vec[0].path, "r");
        	        if (in_file == NULL){
                 	       printf("Error file missing\n");
                 	       exit(-1);
                	}

			char string[50];
                	while ( fscanf(in_file,"%s", string) == 1){

                       		 if(strcmp(string, shared_mem->vec[0].word)==0) {
                        	        shared_mem->vec[0].times+=1;
                        	}
                	}


                fclose(in_file);
		shared_mem->index+=1;
		exit(0);
		}
		
	}
	

		

	for(i=0;i<CHILDS;i++){
		wait(NULL);
		printf("Found the word %s in the file %s , %d times\n",shared_mem->vec[i].word,shared_mem->vec[i].path,shared_mem->vec[i].times);

	}
	
	

	/* Desfaz o mapeamento */
	r = munmap(shared_mem, sizeof(shared_mem));

	if (r < 0) {
		perror("No munmap()!");
		exit(1);
	}

	return 0;
}




