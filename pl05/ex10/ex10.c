#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>
#include <unistd.h>

#define SIZE 10000

int lowest_price = INT_MAX;
int lowest_market;


pthread_mutex_t mux = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mux1= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mux2= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mux3= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mux4= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mux5= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mux6= PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t muxLow= PTHREAD_MUTEX_INITIALIZER;


pthread_cond_t cond1;
pthread_cond_t cond2;
pthread_cond_t cond3;
pthread_cond_t cond4;
pthread_cond_t cond5;


typedef struct{
    int id_h;
    int id_p;
    int p;
}product;

struct DataStruct {
   product vec[SIZE];
};


int indexVec1 = 0;
int indexVec2 = 0;
int indexVec3 = 0;
product vec1[SIZE];
product vec2[SIZE];
product vec3[SIZE];

void* filteringT1(void* arg);
void* filteringT2(void* arg);
void* filteringT3(void* arg);
void* computingT4(void* arg);
void* computingT5(void* arg);
void* computingT6(void* arg);


int main() {
	int i;
    struct DataStruct data;
    pthread_t thread1,thread2,thread3,thread4,thread5,thread6; 
    pthread_cond_init(&cond1,NULL);
    pthread_cond_init(&cond2,NULL);
    pthread_cond_init(&cond3,NULL);
    pthread_cond_init(&cond4,NULL);
    pthread_cond_init(&cond5,NULL);
    
    /*Seed para rand()*/
    srand(time(0)); 

    /*Encher o array com valores aleatórios*/
    int temp_h;
    int temp_id;
    int temp_price;
    for (i = 0; i < SIZE; i++) {
    		temp_h = (rand()% 3)+1;
    		temp_id = (rand()% 5)+1;
    		temp_price = (rand()% 20)+1;
    		data.vec[i].id_h = temp_h;
    		data.vec[i].id_p = temp_id;
    		data.vec[i].p = temp_price;
            //printf("database[%d].key[%d]:%d\n",i,y,data.database[i].key[y]);
    }



	 /*Computing*/
    if(pthread_create(&thread4, NULL, computingT4, &vec1)) {
            perror("Error creating thread.");
    }
	if(pthread_create(&thread5, NULL, computingT5, &vec2)) {
            perror("Error creating thread.");
    }
    if(pthread_create(&thread6, NULL, computingT6, &vec3)) {
            perror("Error creating thread.");
    }

	
 	sleep(5);

    /*Filtering*/

    if(pthread_create(&thread1, NULL, filteringT1, &data)) {
            perror("Error creating thread.");
    }

    if(pthread_create(&thread2, NULL, filteringT2, &data)) {
            perror("Error creating thread.");
    }

    if(pthread_create(&thread3, NULL, filteringT3, &data)) {
            perror("Error creating thread.");
    }

   

   
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread4, NULL);
  	pthread_join(thread5, NULL);
   	pthread_join(thread6, NULL);

    printf("Lowest cost value: %d from hypermarket: %d\n",lowest_price,lowest_market);
 
	return 0;
}


void* filteringT1(void* arg){
	sleep(2);
    pthread_mutex_lock(&mux1); 

	printf("filteringT1\n");
	int i;
	struct DataStruct* data = (struct DataStruct *) arg;
    pthread_mutex_lock(&mux); 
	  for (i = 0; i < 4000; i++) {
	  	if(data->vec[i].id_h==1){
	  		vec1[indexVec1].id_h = data->vec[i].id_h;
	  		vec1[indexVec1].id_p = data->vec[i].id_p;
	  		vec1[indexVec1].p = data->vec[i].p;
	  		indexVec1++;
	  	}else if(data->vec[i].id_h==2){
	  		vec2[indexVec2].id_h = data->vec[i].id_h;
	  		vec2[indexVec2].id_p = data->vec[i].id_p;
	  		vec2[indexVec2].p = data->vec[i].p;
	  		indexVec2++;
	  	}else if(data->vec[i].id_h==3){
	  		vec3[indexVec3].id_h = data->vec[i].id_h;
	  		vec3[indexVec3].id_p = data->vec[i].id_p;
	  		vec3[indexVec3].p = data->vec[i].p;
	  		indexVec3++;
	  	}

	  }
    pthread_mutex_unlock(&mux);
   	

    printf("fim filtering 1\n");
    pthread_mutex_unlock(&mux1);

    printf("T1 sending signal\n");
    pthread_cond_broadcast(&cond1);

    pthread_exit(NULL);  
}

void* filteringT2(void* arg){
	sleep(4);
  	pthread_mutex_lock(&mux2); 

	printf("filteringT2\n");
	int i;
	struct DataStruct* data = (struct DataStruct *) arg;
   	pthread_mutex_lock(&mux); 
	for (i = 4000; i < 7000; i++) {
	  	if(data->vec[i].id_h==1){
	  		vec1[indexVec1].id_h = data->vec[i].id_h;
	  		vec1[indexVec1].id_p = data->vec[i].id_p;
	  		vec1[indexVec1].p = data->vec[i].p;
	  		indexVec1++;
	  	}else if(data->vec[i].id_h==2){
	  		vec2[indexVec2].id_h = data->vec[i].id_h;
	  		vec2[indexVec2].id_p = data->vec[i].id_p;
	  		vec2[indexVec2].p = data->vec[i].p;
	  		indexVec2++;
	  	}else if(data->vec[i].id_h==3){
	  		vec3[indexVec3].id_h = data->vec[i].id_h;
	  		vec3[indexVec3].id_p = data->vec[i].id_p;
	  		vec3[indexVec3].p = data->vec[i].p;
	  		indexVec3++;
	  	}
	}
	pthread_mutex_unlock(&mux);
  	
	printf("fim filtering 2\n");
	pthread_mutex_unlock(&mux2);

	printf("T2 sending signal\n");

	pthread_cond_broadcast(&cond2);


    pthread_exit(NULL); 	  
}

void* filteringT3(void* arg){
	sleep(6);
    pthread_mutex_lock(&mux3);

	printf("filteringT3\n");
	
	int i;
	struct DataStruct* data = (struct DataStruct *) arg; 
    pthread_mutex_lock(&mux);
	for (i = 7000; i < 10000; i++) {
		//printf("T3 i:%d\n",i);
	  	if(data->vec[i].id_h==1){
	  		vec1[indexVec1].id_h = data->vec[i].id_h;
	  		vec1[indexVec1].id_p = data->vec[i].id_p;
	  		vec1[indexVec1].p = data->vec[i].p;
	  		indexVec1++;
	  	}else if(data->vec[i].id_h==2){
	  		vec2[indexVec2].id_h = data->vec[i].id_h;
	  		vec2[indexVec2].id_p = data->vec[i].id_p;
	  		vec2[indexVec2].p = data->vec[i].p;
	  		indexVec2++;
	  	}else if(data->vec[i].id_h==3){
	  		vec3[indexVec3].id_h = data->vec[i].id_h;
	  		vec3[indexVec3].id_p = data->vec[i].id_p;
	  		vec3[indexVec3].p = data->vec[i].p;
	  		indexVec3++;
	  	}
	}

  	pthread_mutex_unlock(&mux);
  	

    printf("Fim filtering 3\n");
    pthread_mutex_unlock(&mux3);

    printf("T3 sending signal\n");
	pthread_cond_broadcast(&cond3);

    pthread_exit(NULL); 	   
}

void* computingT4(void* arg){
	sleep(1);
	int i;
	int sum1=0;
	int sum2=0;
	int sum3=0;
	int sum4=0;
	int sum5=0;

	int n_prod1=0;
	int n_prod2=0;
	int n_prod3=0;
	int n_prod4=0;
	int n_prod5=0;

    int avg_prod1=0;
    int avg_prod2=0;
    int avg_prod3=0;
    int avg_prod4=0;
    int avg_prod5=0;

    float total_avg=0;

	printf("T4 b4 con1\n");

	pthread_cond_wait(&cond1,&mux1);
	printf("T4 after con1\n");

	printf("T4 b4 con2\n");

	pthread_cond_wait(&cond2,&mux2);
	printf("T4 after cond2\n");

	printf("T4 b4 con3\n");

	pthread_cond_wait(&cond3,&mux3);
	printf("T4 after cond3\n");

	

	printf("computingT4\n");

	for(i=0;i<indexVec1;i++){
		int t_id=vec1[i].id_p;
		switch(t_id){
			case 1:
			sum1+=vec1[i].p;
			n_prod1++;
			break;

			case 2:
			sum2+=vec1[i].p;
			n_prod2++;			
			break;
			
			case 3:
			sum3+=vec1[i].p;
			n_prod3++;
			break;
			
			case 4:
			sum4+=vec1[i].p;
			n_prod4++;
			break;
			
			case 5:
			sum5+=vec1[i].p;
			n_prod5++;
			break;
		}


	}

	avg_prod1=sum1/n_prod1;
	avg_prod2=sum2/n_prod2;
	avg_prod3=sum3/n_prod3;
	avg_prod4=sum4/n_prod4;
	avg_prod5=sum5/n_prod5;

	pthread_mutex_lock(&muxLow);	
	printf("T4 after lock\n");

	total_avg=avg_prod1+avg_prod2+avg_prod3+avg_prod4+avg_prod5;
	if(total_avg<lowest_price){
		lowest_price=total_avg;
		lowest_market=1;
	}
	pthread_mutex_unlock(&muxLow);
	printf("finish computingT4\n");


    pthread_exit(NULL);

}


void* computingT5(void* arg){
	sleep(2);
	int i;
	int sum1=0;
	int sum2=0;
	int sum3=0;
	int sum4=0;
	int sum5=0;

	int n_prod1=0;
	int n_prod2=0;
	int n_prod3=0;
	int n_prod4=0;
	int n_prod5=0;

    int avg_prod1=0;
    int avg_prod2=0;
    int avg_prod3=0;
    int avg_prod4=0;
    int avg_prod5=0;

    float total_avg=0;

    printf("T5 b4 con1\n");

	pthread_cond_wait(&cond1,&mux1);
	printf("T5 after con1\n");

	printf("T5 b4 con2\n");

	pthread_cond_wait(&cond2,&mux2);
	printf("T5 after cond2\n");

	printf("T5 b4 con3\n");

	pthread_cond_wait(&cond3,&mux3);
	printf("T5 after cond3\n");


	printf("computingT5\n");

	for(i=0;i<indexVec2;i++){
		int t_id=vec2[i].id_p;
		switch(t_id){
			case 1:
			sum1+=vec2[i].p;
			n_prod1++;
			break;

			case 2:
			sum2+=vec2[i].p;
			n_prod2++;			
			break;
			
			case 3:
			sum3+=vec2[i].p;
			n_prod3++;
			break;
			
			case 4:
			sum4+=vec2[i].p;
			n_prod4++;
			break;
			
			case 5:
			sum5+=vec2[i].p;
			n_prod5++;
			break;
		}


	}

	avg_prod1=sum1/n_prod1;
	avg_prod2=sum2/n_prod2;
	avg_prod3=sum3/n_prod3;
	avg_prod4=sum4/n_prod4;
	avg_prod5=sum5/n_prod5;

	total_avg=avg_prod1+avg_prod2+avg_prod3+avg_prod4+avg_prod5;
	pthread_mutex_lock(&muxLow);	

	if(total_avg<lowest_price){
		lowest_price=total_avg;
		lowest_market=2;
	}

	pthread_mutex_unlock(&muxLow);


    printf("finish computingT5\n");


    pthread_exit(NULL);

}


void* computingT6(void* arg){
	sleep(3);
	int i;
	int sum1=0;
	int sum2=0;
	int sum3=0;
	int sum4=0;
	int sum5=0;

	int n_prod1=0;
	int n_prod2=0;
	int n_prod3=0;
	int n_prod4=0;
	int n_prod5=0;

    int avg_prod1=0;
    int avg_prod2=0;
    int avg_prod3=0;
    int avg_prod4=0;
    int avg_prod5=0;

    float total_avg=0;
    printf("T6 b4 con1\n");

  	pthread_cond_wait(&cond1,&mux1);
	printf("T6 after con1\n");

	printf("T6 b4 con2\n");

	pthread_cond_wait(&cond2,&mux2);
	printf("T6 after cond2\n");

	printf("T6 b4 con3\n");

	pthread_cond_wait(&cond3,&mux3);
	printf("T6 after cond3\n");

	printf("computingT6\n");

	for(i=0;i<indexVec3;i++){
		int t_id=vec3[i].id_p;
		switch(t_id){
			case 1:
			sum1+=vec3[i].p;
			n_prod1++;
			break;

			case 2:
			sum2+=vec3[i].p;
			n_prod2++;			
			break;
			
			case 3:
			sum3+=vec3[i].p;
			n_prod3++;
			break;
			
			case 4:
			sum4+=vec3[i].p;
			n_prod4++;
			break;
			
			case 5:
			sum5+=vec3[i].p;
			n_prod5++;
			break;
		}


	}

	avg_prod1=sum1/n_prod1;
	avg_prod2=sum2/n_prod2;
	avg_prod3=sum3/n_prod3;
	avg_prod4=sum4/n_prod4;
	avg_prod5=sum5/n_prod5;

	pthread_mutex_lock(&muxLow);	
	total_avg=avg_prod1+avg_prod2+avg_prod3+avg_prod4+avg_prod5;
	if(total_avg<lowest_price){
		lowest_price=total_avg;
		lowest_market=3;
	}
	pthread_mutex_unlock(&muxLow);

    printf("finish computingT6\n");


    pthread_exit(NULL);

}



