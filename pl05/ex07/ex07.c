#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>

#define SIZE 8000
#define ELEM 16

pthread_mutex_t mux = PTHREAD_MUTEX_INITIALIZER;


typedef struct{
    int key[5];
}keys;

struct DataStruct {
    keys database[SIZE];
    int lower_limit;
    int statistics[49];
};

void* calculateStatistic(void* arg) {
    pthread_mutex_lock(&mux);
    struct DataStruct* data = (struct DataStruct *) arg; 
    int i,y;
    int number;
    int upper_limit = data->lower_limit+500;
    for(i=data->lower_limit;i<upper_limit;i++){
         for(y=0;y<5;y++){
            number=data->database[i].key[y];
            //printf("Number: %d\n",number);
            data->statistics[number-1]++;
        }
    }
    data->lower_limit+=500;
    pthread_mutex_unlock(&mux);
    pthread_exit(NULL);
}


int main() {
    int i,y;
    struct DataStruct data;
    pthread_t thread_ids[ELEM];

    /*Seed para rand()*/
    srand(time(0)); 

    /*Encher a database com valores aleatórios*/
    for (i = 0; i < SIZE; i++) {
        for(y=0;y<5;y++){
            data.database[i].key[y] = (rand()% 49)+1;
            //printf("database[%d].key[%d]:%d\n",i,y,data.database[i].key[y]);
        }
    }
    data.lower_limit=0;
    /*Por o array de estatisticas a zero */
    for(i=0;i<49;i++){
        data.statistics[i]=0;
    }


    for(i = 0; i < ELEM; i++) {
        if(pthread_create(&thread_ids[i], NULL, calculateStatistic, &data)) {
            perror("Error creating thread.");
        }
    }

    for(i = 0; i <ELEM; i++) {
        pthread_join(thread_ids[i], NULL);
    }

    
    for(i = 0; i < 49; i++) {
        printf("O número %d apareceu %d vezes\n",i+1,data.statistics[i]);
    }

    return 0;
}