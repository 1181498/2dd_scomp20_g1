#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <time.h>


int main(int argc, char *argv[]) {
    
    int i;	
    sem_t *semaforo, *semaforo2; /* ap para o semaforo */
    pid_t pid;

         
    /* criar o semáforo com valor = 1 */
	if ((semaforo = sem_open("semaforo", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
        	perror("No sem_open()");
        	exit(1);
    	}

 	/* criar o semáforo com valor = 1 */
	if ((semaforo2 = sem_open("semaforo2", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
        	perror("No sem_open()");
        	exit(1);
    	}
    
	pid=fork();
	
	for (i = 0; i < 10; i++){
		if (pid>0)/* Se é o pai */{
			sem_wait(semaforo);
			printf("S");
			fflush(stdout);
			sem_post(semaforo2);
		}
		else{/*Filho*/	
			sem_wait(semaforo2);
			printf("C");
			fflush(stdout);
			sem_post(semaforo);
		}
	}
	 
	


	wait(NULL);
    
    /* fechar/apagar o semaforo */
	if(sem_close(semaforo) < 0) {
        	perror("sem_close()");
        	exit(EXIT_FAILURE);
	}
 
	if (sem_unlink("semaforo") < 0) {
        	perror("sem_unlink()");
        	exit(EXIT_FAILURE);
	}
    /* fechar/apagar o semaforo */
	if(sem_close(semaforo2) < 0) {
        	perror("sem_close()");
        	exit(EXIT_FAILURE);
	}
 
	if (sem_unlink("semaforo2") < 0) {
        	perror("sem_unlink()");
        	exit(EXIT_FAILURE);
	}

	return 0;
}
