#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>

int main (void)
{
	
	sem_t* sem;
	sem_t* sem2;
	
	pid_t p;
	
	if ((sem = sem_open("semaforo", O_CREAT | O_EXCL, 0600, 0)) == SEM_FAILED) {
		printf("Semáforo já existe\n");
		return -1;
	}
	
	if ((sem2 = sem_open("semaforo2", O_CREAT | O_EXCL, 0600, 0)) == SEM_FAILED) {
		printf("Semáforo já existe\n");
		return -1;
	}
	
	p = fork();
	
	if(p!=0){		
		buyChips();
		sem_post(sem);
		sem_wait(sem2);
		eatAndDrink(p);
		
	}else{	
		buyBeer();
		sem_post(sem2);
		sem_wait(sem);
		eatAndDrink(p);
	}	

	wait(NULL);
	sem_unlink("semaforo");	
	sem_unlink("semaforo2");	
	return 0;
}

int buyChips(){
	printf("I just bought chips!\n");	
	return 0;
}

int buyBeer(){
	printf("I just bought beer!\n");	
	return 0;
}

int eatAndDrink(pid_t p){
	if(p!=0){
	printf("The parent ate!\n");
	}else{
	printf("The child ate!\n");
	}
return 0;
}

