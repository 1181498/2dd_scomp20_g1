#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>


	
int main (void)
{
	
	sem_t* sem;
	pid_t p;
	FILE *f;
	int i;
	int numbers[200];
	sem = sem_open("semaforo",O_CREAT|O_EXCL,0644,1);
	
	if(sem==SEM_FAILED){
		printf("Semaforo error!\n");
		return 0;
	}	

	for(i=0;i<8;i++){
		p = fork();
		
		if(p==0){
			f=fopen("Numbers.txt","r");
			int aux;
			for(i=0;i<200;i++){
				fscanf(f,"%d ",&aux);
				numbers[i] = aux;
			}
			fclose(f);
			sem_wait(sem);
			f=fopen("Output.txt","a");
			for(i=0;i<200;i++){
				fprintf(f,"%d ",numbers[i]);
			}
			fprintf(f,"\n\n");
			fclose(f);
			sem_post(sem);
			
			exit(1);
		}
	}
	for(i=0;i<8;i++){
		wait(NULL);
	}
	sem_unlink("semaforo");	
 return 0;
}
