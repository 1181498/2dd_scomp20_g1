#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#define N 8	
#define MAX_THREAD 8

//Counter for threads
int step_i=0;

int matA[N][N];
int matB[N][N];
int matC[N][N];

void print_result(int matrix[N][N],char letra){
    printf("Result matrix %c is \n\n",letra); 
    for (int i = 0; i < N; i++) { 
        for (int j = 0; j < N; j++){
            printf("%d ", matrix[i][j]);
        } 
        printf("\n"); 
    }
}

void fill_matrix(void *arg) {
    int (*data)[N][N] = (int(*)[N][N]) arg;
    // Generating random values in mat
    for (int i = 0; i < N; i++) {   
        for (int j = 0; j < N; j++){
            (*data)[i][j]=(rand() % 5) + 1;
        }
    } 
    pthread_exit(NULL);
}

struct DataStruct{
    int matrixA[N][N];
    int matrixB[N][N];
    int matrixRes[N][N];
};

void* multiply(void* arg) {
    struct DataStruct* data = (struct DataStruct *) arg;
    int core = step_i++;   
    // Each thread computes 1/Nth of matrix multiplication cause the size of matrix is NxN
    for (int i = core * N / 8; i < (core + 1) * N / 8; i++)  
        for (int j = 0; j < N; j++)  
            for (int k = 0; k < N; k++)  
                data->matrixRes[i][j] += data->matrixA[i][j] * data->matrixB[i][j];        
    pthread_exit(NULL);            
}

int main(){

    pthread_t threadA,threadB;

    struct DataStruct data;

    for (int i = 0; i < N; i++) {   
        for (int j = 0; j < N; j++){
            data.matrixRes[i][j]=0;
        }
    } 

    pthread_create(&threadA, NULL, fill_matrix,(void*)(data.matrixA));
    pthread_create(&threadB, NULL, fill_matrix,(void*)(data.matrixB));
    pthread_join(threadA, NULL);
    pthread_join(threadB, NULL);
    print_result(data.matrixA,'A');
    printf("\n=======================\n");
    print_result(data.matrixB,'B');


    // declaring n threads 
    pthread_t thread_ids[MAX_THREAD];
    int i;

    // Creating n threads, each evaluating its own part 
    for (i = 0; i < MAX_THREAD; i++) { 
        if(pthread_create(&thread_ids[i], NULL, multiply,(void*)&data)) {
            perror("Error creating thread.");
        }
    }

    // joining and waiting for all threads to complete 
    for(i = 0; i <MAX_THREAD; i++) {
        pthread_join(thread_ids[i], NULL);
    }

    printf("\n=======================\n");
    print_result(data.matrixRes,'C');
    return 0;
}
