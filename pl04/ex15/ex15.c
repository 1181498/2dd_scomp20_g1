#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>

	sem_t* sem;
	sem_t* sem2;
	sem_t* sem3;


int allunlink(){
	sem_unlink("semaforo");	
	sem_unlink("semaforo2");
	sem_unlink("semaforo3");	
	shm_unlink("/xpto");
	return 0;
}

int decorrershow(){

	int i;
	printf("Doors opening!\n");
	sem_post(sem);
	sleep(1);
	sem_wait(sem);
	printf("Show started!\n");
	for(i=0;i<5;i++){
		sleep(1); 
		printf("%d seconds\n",i);
	}
	
	printf("Show ended!\n");
	printf("Doors opening!\n");
	sem_post(sem2);
	sleep(1);
	sem_wait(sem2);
	
	for(i=0;i<5;i++){
		sleep(1); 
		printf("%d seconds for the next one\n",i);
	}
	
	return 0;
}	

int entershowroom(pid_t pid){
	if(sem_trywait(sem3)!=-1){
	sem_wait(sem);
	sem_post(sem);
	printf("I (%d) have entered the showroom\n", pid);
	leaveshowroom(pid);
	}
	return 0;
	
}

int leaveshowroom(pid_t pid){
	sem_wait(sem2);
	sem_post(sem2);

	printf("I (%d) have left the showroom\n", pid);		
	sem_post(sem3);	
	return 0;
}
	
	
int main (void)
{
	pid_t p;
	
	//Semaforo de mutual exclusion para representar as portas de entrada
	sem  = sem_open("semaforo",O_CREAT|O_EXCL,0600,0);
	
	//Semaforo de mutual exclusion para representar as portas de saida
	sem2  = sem_open("semaforo2",O_CREAT|O_EXCL,0600,0);
	
	//Semaforo para blockear a sala se estiver cheia
	sem3 = sem_open("semaforo3",O_CREAT|O_EXCL,0600,5);
	
	
	if(sem==SEM_FAILED || sem2==SEM_FAILED || sem3==SEM_FAILED){
		printf("Semaforo error!\n");
		allunlink();
		return 0;
	}
	
	p = fork();
	
	if(p==0){
		while(1){
			decorrershow();
		}
		exit(1);
	}else{
	
	
	int i;
	for(i=0;i<6;i++){			
	p = fork();
	
		if(p==0){
			while(1){
				entershowroom(getpid());
			}
		
			exit(1);
		}else{
			
		}
	}
	}

	
	wait(NULL);
	wait(NULL);
	wait(NULL);
	wait(NULL);
	wait(NULL);
	wait(NULL);
	
	allunlink();
	printf("\n");

	return 0;
}




