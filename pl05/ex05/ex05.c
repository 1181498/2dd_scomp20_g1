#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>

#define SIZE 1000

int lowest_balance;
int highest_balance;
float average_balance;


struct DataStruct {
    int accounts[SIZE];
};

void* getLowestBalance(void* arg) {
    struct DataStruct* data = (struct DataStruct *) arg;
    int i;
    for(i=0;i<SIZE;i++){
        if(data->accounts[i]<lowest_balance){
            lowest_balance=data->accounts[i];
        }
    }
    pthread_exit(NULL);
}

void* getHighestBalance(void* arg) {
    struct DataStruct* data = (struct DataStruct *) arg;
    int i;
    for(i=0;i<SIZE;i++){
        if(data->accounts[i]>highest_balance){
            highest_balance=data->accounts[i];
        }
    }
    pthread_exit(NULL);
}


void* getAverageBalance(void* arg) {
    struct DataStruct* data = (struct DataStruct *) arg;
    int i;
    int sum=0;
    for(i=0;i<SIZE;i++){
        sum+=data->accounts[i];
    }
    average_balance=sum/SIZE;
    pthread_exit(NULL);
}



int main() {
    int i;
    struct DataStruct data;
    pthread_t threadLow, threadHigh, threadAverage;

    /*Inicialização das variáveis globais*/
    lowest_balance=INT_MAX;
    highest_balance=INT_MIN;
    average_balance=0;

    /*Seed para rand()*/
    srand(time(0)); 

    /*Encher as accounts com valores aleatórios*/
    for (i = 0; i < SIZE; i++) {
        data.accounts[i] = rand() % 1000001;
    }
    
    if(pthread_create(&threadLow, NULL, getLowestBalance, &data)) {
            perror("Error creating thread.");
    }
    
    if(pthread_create(&threadHigh, NULL, getHighestBalance, &data)) {
            perror("Error creating thread.");
    }

    if(pthread_create(&threadAverage, NULL, getAverageBalance, &data)) {
            perror("Error creating thread.");
    }

    pthread_join(threadLow, NULL);
    pthread_join(threadHigh, NULL);
    pthread_join(threadAverage, NULL);


    printf("Out of %d accounts, the lowest balance was %d ,",SIZE,lowest_balance);
    printf(" the highest balance was %d and the average balance was %f\n",highest_balance,average_balance);   
 
    return 0;
}