#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <time.h>

#define STRING_SIZE 80
#define NUMBER_OF_STRINGS 50
#define CHILDS 5
#define WAIT_TIME 1

/*
Sources:
https://www.oreilly.com/library/view/c-in-a/0596006977/re210.html

*/

typedef struct{
	char s[STRING_SIZE];
}string_data_type;


typedef struct {
	int index;
	string_data_type arr[NUMBER_OF_STRINGS];
} shm_data_type;


int main(int argc, char *argv[]) {
    shm_data_type *shm_counter;
    int shmfd; /* descritor da memória partilhada */
    int i, r,s,myInteger;	
    sem_t *semaforo; /* ap para o semaforo */
    pid_t pid;
    int size = 64;	
 
    struct timespec t1;
    t1.tv_sec=WAIT_TIME;		
        
    /* criar o objeto de memoria partilhada */
    shmfd = shm_open("/hsp_shm", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
    if (shmfd < 0) {
        perror("No shm_open()");
        exit(1);
    }
    
    /* ajustar o tamanho da mem. partilhada */
    ftruncate(shmfd, sizeof(shm_data_type));
    
    /* mapear a mem. partilhada */
    shm_counter = (shm_data_type *)mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
    if (shm_counter == NULL) {
        perror("No mmap()");
        exit(1);
    }
    
    /* criar o semáforo com valor = 1 */
    if ((semaforo = sem_open("semaforo", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
        perror("No sem_open()");
        exit(1);
    }
    

     shm_counter->index=0;	
	
    
     for(s=0;s<CHILDS;s++){
	pid = fork(); /* fork */
	if (pid == 0) { /* filho */
        	sem_timedwait(semaforo,&t1);
		char buffer[64];
		snprintf(buffer, 64,"I’m the Father - with PID %d",getpid());			

		int temp_index= shm_counter->index;
        	strcpy(shm_counter->arr[temp_index].s,buffer);
    		//printf("%s\n",shm_counter->arr[temp_index].s);
        	shm_counter->index+=1;
		
		sem_post(semaforo);
        	exit(0);
    	}		

     }                   

    
    
    
    /* pai */
    sem_timedwait(semaforo,&t1);
		
    char buffer[64];
    snprintf(buffer, 64,"I’m the Father - with PID %d",getpid());			
    int temp_index= shm_counter->index;

    strcpy(shm_counter->arr[temp_index].s,buffer);
    //printf("%s\n",shm_counter->arr[temp_index].s);
    shm_counter->index+=1;
		
    sem_post(semaforo);
    
    wait(NULL); /* esperar que o filho termine */

    
    for(i=0;i<CHILDS;i++){
	printf("Hello: %s\n",shm_counter->arr[i].s);	
     }	

    
    /* fechar/apagar o semaforo */
	if(sem_close(semaforo) < 0) {
        perror("sem_close()");
        exit(EXIT_FAILURE);
	}
 
	if (sem_unlink("semaforo") < 0) {
        perror("sem_unlink()");
        exit(EXIT_FAILURE);
	}
    
    /* desfaz mapeamento */
    r=munmap(shm_counter, sizeof(shm_counter));
    if (r < 0) { /* verifica erro */
        perror("No munmap()");
        exit(1);
    }
    
    /* apaga a memoria partilhada do sistema */
    r=shm_unlink("//hsp_shm");
    if (r < 0) { /* verifica erro */
        perror("No unlink()");
        exit(1);
    }
    
    return 0;
}
