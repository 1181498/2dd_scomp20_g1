#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>

#define TIMES 100

sem_t* sem;
sem_t* sem2;
sem_t* sem3;
int capacity = 200;
int currentPass=0;


int leaveTrain(int porta){
	printf("Pessoas no comboio:%d\n",currentPass);
	if(currentPass!=0){
		currentPass--;
		switch(porta){
			case 1 :{
				sem_wait(sem);
				printf("Alguem saiu na porta 1\n");
				sem_post(sem);
				break;
				}
			case 2 :{
				sem_wait(sem2);
				printf("Alguem saiu na porta 2\n");
				sem_post(sem2);
				break;
				}
			case 3 :{
				sem_wait(sem3);
				printf("Alguem saiu na porta 3\n");
				sem_post(sem3);
				break;
				}
	}
	}else{
		printf("Comboio vazio\n");
	}
	return 0;
}

int enterTrain(int porta){
	printf("Pessoas no comboio:%d\n",currentPass);
	if(currentPass<capacity){
		currentPass++;	
		switch(porta){
			case 1 :{
				sem_wait(sem);
				printf("Alguem entrou na porta 1\n");
				sem_post(sem);
			break;
			}
			case 2 :{
				sem_wait(sem2);
				printf("Alguem entrou na porta 2\n");
				sem_post(sem2);
			break;
			}
			case 3 :{
				sem_wait(sem3);
				printf("Alguem entrou na porta 3\n");
				sem_post(sem3);
			break;
			}
		}
	}else if(currentPass=capacity){
		printf("Comboio cheio\n");
	}
	return 0;
}



	
int main (void)
{
	/* criar o semáforo com valor = 1 */
    	if ((sem = sem_open("sem", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
        perror("No sem_open()");
        exit(1);
    	}
	/* criar o semáforo com valor = 1 */
    	if ((sem2 = sem_open("sem2", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
        perror("No sem_open()");
        exit(1);
    	}
	/* criar o semáforo com valor = 1 */
    	if ((sem3 = sem_open("sem3", O_CREAT | O_EXCL, 0644, 1)) == SEM_FAILED) {
        perror("No sem_open()");
        exit(1);
    	}
	
	
	
	int i=0;
	int r=0;
	int randomDoor=0;
	for(i=0;i<TIMES;i++){
	     r = rand() % 2; /* 0 entra e 1 saí*/
	     randomDoor = (rand() %3)+1; 
	     if(r==0){
		enterTrain(randomDoor);
	     }else{
		leaveTrain(randomDoor);
	     }					

	}

	
	
	

	
    /* fechar/apagar o semaforo */
	if(sem_close(sem) < 0) {
        perror("sem_close()");
        exit(EXIT_FAILURE);
	}
 
	if (sem_unlink("sem") < 0) {
        perror("sem_unlink()");
        exit(EXIT_FAILURE);
	}
	
    /* fechar/apagar o semaforo */
	if(sem_close(sem2) < 0) {
        perror("sem_close()");
        exit(EXIT_FAILURE);
	}
 
	if (sem_unlink("sem2") < 0) {
        perror("sem_unlink()");
        exit(EXIT_FAILURE);
	}
	
    /* fechar/apagar o semaforo */
	if(sem_close(sem3) < 0) {
        perror("sem_close()");
        exit(EXIT_FAILURE);
	}
 
	if (sem_unlink("sem3") < 0) {
        perror("sem_unlink()");
        exit(EXIT_FAILURE);
	}
	return 0;
}



