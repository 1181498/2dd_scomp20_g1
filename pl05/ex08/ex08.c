#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/time.h>
#include <semaphore.h>
#include <pthread.h>

#define SIZE 1000	
#define THREADS 5


pthread_mutex_t mux = PTHREAD_MUTEX_INITIALIZER;

struct DataStruct{
	int data[SIZE];
	int result[SIZE];
	int lower_limit;
};

void* calculation(void* args);

int main(){
	pthread_t threads[THREADS];
	struct DataStruct args;
	int i;
	args.lower_limit = 0;
	
	/*Seed para rand()*/
    srand(time(0)); 
	
	for(i = 0; i < SIZE; i++){
		args.data[i] = (rand()% 49)+1;
	}
	
	for(i = 0; i < THREADS; i++){
		pthread_create(&threads[i], NULL, calculation, (void*) &args);
	}
	
	for (i = 0; i < THREADS; i++){	
		pthread_join(threads[i], NULL);
	}
	
	return 0;	
	
}

void* calculation(void* args){
	pthread_mutex_lock(&mux);
	struct DataStruct* data_struct = (struct DataStruct *) args;
	int i = 0, upper_limit = data_struct->lower_limit + SIZE/THREADS;
	
	for(i = data_struct->lower_limit; i < upper_limit; i++){
		data_struct->result[i] = data_struct->data[i] * 10 + 2;
	}
	
	for(i = data_struct->lower_limit; i < upper_limit; i++){
		printf("result[%d] = %d * 10 + 2 = %d\n", i, data_struct->data[i], data_struct->result[i]);
	}
	
	data_struct->lower_limit += SIZE/THREADS;
	
	pthread_mutex_unlock(&mux);
	pthread_exit(NULL);
}