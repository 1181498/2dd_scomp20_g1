#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <limits.h>

#define BIG_ARRAY 1000
#define ARRAY_SIZE 10
#define MINIMUM 0
#define MAXIMUM 1000
#define CHILD_NUM 10

int vec_max(int limit,int *big,int *small){
	int inf=limit-100;
	int sup=limit;	
	int i=0;
	int pos;
	int max = INT_MIN;
	for(i=inf;i<sup;i++){
		if(big[i]>max){
			max=big[i];
		}				
	} 
	return max;
}

typedef struct {
	int limit;
	int array[ARRAY_SIZE];
} shm_data_type;

int main() {
	int big_array[BIG_ARRAY];
	int fd, i, r,p,temp;
	int fd1[2];
	time_t t;
	shm_data_type *shared_mem;

	/* read,write,execute/search permissions by owner and group */
	fd = shm_open("/shmHSP", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);

	/* Verifica se a memória partilhada foi devidamente aberta */
	if (fd < 0) {
		perror("No smh_open!");
		exit(0);
	}

	/* Ajusta o tamanho da memória partilhada */
	ftruncate(fd, sizeof(shm_data_type));

	/* Mapea a memória partilhada */
	shared_mem = (shm_data_type *) mmap(NULL, sizeof(shm_data_type), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);

	/* Verifica se a memória partilhada foi devidamente mapeada */
	if (shared_mem == NULL) {
		printf("No mmap()");
		exit(0);
	}

	for (i = 0; i < ARRAY_SIZE; i++) {
		shared_mem->array[i] = 0;
		//printf("\nNUMBER %d = %d", (i + 1), shared_mem->array[i]);
	}	
	shared_mem->limit=100;

	/* Inicializa o gerador aleatório */
	srand((unsigned) time(&t));

	/* Preenche o array com números aleatórios */
	for (i = 0; i < BIG_ARRAY; i++) {
		big_array[i] = rand() % 1001;
		//printf("\nNUMBER %d = %d", (i + 1), shared_mem->array[i]);
	}
	printf("\n\n");
	

	pipe(fd1);	


	for(i=0;i<CHILD_NUM;i++){
		p = fork();
		if(p==0){  //filho
			//printf("Limit : %d\n",shared_mem->limit);
			temp = vec_max(shared_mem->limit,big_array,shared_mem->array);
			shared_mem->limit+=100;
			write(fd1[1], &temp, sizeof(temp));
			exit(0);
		}
	}	

	int s;
	int num;
	int m;
	for(r=0;r<CHILD_NUM;r++){
		wait(&s);
		read(fd1[0], &num, sizeof(num));
		printf("num:%d\n",num);
		shared_mem->array[r]=num;
			
	}
	m = INT_MIN;
	for(i=0;i<ARRAY_SIZE;i++){
		if(shared_mem->array[i]>m){
			m=shared_mem->array[i];
		}				
	} 

	printf("Global max: %d\n",m);




	/* Desfaz o mapeamento */
	r = munmap(shared_mem, sizeof(shared_mem));

	if (r < 0) {
		perror("No munmap()!");
		exit(1);
	}

	return 0;
}




