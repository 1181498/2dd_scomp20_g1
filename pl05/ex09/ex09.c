#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#define MAX_THREAD 4

typedef struct{
	char nome;
}cidade;

typedef struct{
	cidade origem;
	cidade destino;	
	pthread_mutex_t mux;	
}estrada;

typedef struct{
	estrada num[3]; 
}caminhos;

caminhos caminho;

void* viagem(void* arg){
	
	int index = *(int*) arg;
	int timer = (rand() % 10) + 1;
	pthread_mutex_lock(&caminho.num[index].mux);

	printf("O comboio %lu esta viagem entre a cidade %c e %c.\n",
		pthread_self(),
		caminho.num[index].origem.nome,
		caminho.num[index].destino.nome);
	
	sleep(timer);

	printf("O comboio %lu terminou a viagem entre a cidade %c e %c.\n",
		pthread_self(),
		caminho.num[index].origem.nome,
		caminho.num[index].destino.nome);

	pthread_mutex_unlock(&caminho.num[index].mux);

	pthread_exit((int*)timer);
}

int main(){
	pthread_t comboios[MAX_THREAD];

	//preencher as estruturas com as cidades
	caminho.num[0].origem.nome = 'A';
	caminho.num[0].destino.nome = 'B';

	caminho.num[1].origem.nome = 'B';
	caminho.num[1].destino.nome = 'C';

	caminho.num[2].origem.nome = 'B';
	caminho.num[2].destino.nome = 'D';

	//criar comboio / viagem
	int escolha[MAX_THREAD];
	for(int i=0;i<MAX_THREAD;i++){
		escolha[i] = rand() % 3;
		if(pthread_create(&comboios[i], NULL, viagem,&escolha[i])) {
            perror("Error creating thread.");
        }
	}

	// joining and waiting for all threads to complete
	int timer;
    for(int i = 0; i <MAX_THREAD; i++) {
        pthread_join(comboios[i],(int*) &timer);
        printf("Train %lu finished the trip in %d seconds!\n ",comboios[i],timer);
    }

    //destroying all the mutex
    for(int i=0;i<3;i++){
		pthread_mutex_destroy(&caminho.num[i].mux);
	}

	return 0;
}