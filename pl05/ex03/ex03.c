#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>

#define ELEM 10
#define SIZE 1000
#define FIND 555


int array[SIZE];



void* funcSearch(void *arg) {
    int i;
    int lower_limit = (int) arg;
    int upper_limit = lower_limit + 100;
    printf("Thread %d searching from %d to %d\n",pthread_self(),lower_limit,upper_limit-1);
    for(i=lower_limit;i<upper_limit;i++){
        if(array[i]==FIND){
            printf("Number %d found in the position: %d\n",FIND,i);
            pthread_exit((void*)pthread_self());

        }
    }
    pthread_exit(NULL);
}

int main() {
    int i;
    int lower_limit=0;
    pthread_t thread_ids[ELEM];

    /*Popular o array, pode ser alterado*/
    for(i=0;i<SIZE;i++){
        array[i]=i;
    }


    for(i = 0; i < ELEM; i++) {
        if(pthread_create(&thread_ids[i], NULL, funcSearch, (void*) lower_limit)) {
            perror("Error creating thread.");
        }
        lower_limit+=100;
    }

    void* thread;
    void* found_thread;

    for(i = 0; i <ELEM; i++) {
        pthread_join(thread_ids[i], (void*) &thread);
        if(thread!=NULL){
            found_thread = thread;
        }
    }
    printf("Thread: %d was the one that found the number %d\n",(int)found_thread,FIND);

    return 0;
}