#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <semaphore.h>

int main (void)
{
	sem_t* sem[8];
	pid_t p;
	FILE *f;
	int i;
	int numbers[200];
	
	sem[0] = sem_open("semaforo1",O_CREAT|O_EXCL,0644,1);
	sem[1] = sem_open("semaforo2",O_CREAT|O_EXCL,0644,0);
	sem[2] = sem_open("semaforo3",O_CREAT|O_EXCL,0644,0);
	sem[3] = sem_open("semaforo4",O_CREAT|O_EXCL,0644,0);
	sem[4] = sem_open("semaforo5",O_CREAT|O_EXCL,0644,0);
	sem[5] = sem_open("semaforo6",O_CREAT|O_EXCL,0644,0);
	sem[6] = sem_open("semaforo7",O_CREAT|O_EXCL,0644,0);
	sem[7] = sem_open("semaforo8",O_CREAT|O_EXCL,0644,0);
	
	for(i=0;i<8;i++){
		if(sem[i]==SEM_FAILED){
			printf("Semaforo error!\n");
			return 0;
		}	
	}
	
	for(i=0;i<8;i++){
		p = fork();
		if(p==0){
			sem_wait(sem[i]);
			f=fopen("Numbers.txt","r");

			if(f==NULL){
				printf("Erro ao aceder ao ficheiro de input\n");
				exit(i);
			}
			
			int aux;
			for(i=0;i<200;i++){
				fscanf(f,"%d ",&aux);
				numbers[i] = aux;
			}
			fclose(f);
			f=fopen("Output.txt","a");
			
			if(f==NULL){
				printf("Erro ao aceder ao ficheiro de output\n");
				exit(i);
			}
			
			for(i=0;i<200;i++){
				fprintf(f,"%d ",numbers[i]);
			}
			fprintf(f,"\n\n");
			fclose(f);
			
			if(i<7){
			sem_post(sem[i+i]);
			}else{
			sem_post(sem[i]);
			}
			
			exit(i);
		}
	}
	for(i=0;i<8;i++){
		wait(NULL);
	}
	sem_unlink("semaforo1");
	sem_unlink("semaforo2");
	sem_unlink("semaforo3");
	sem_unlink("semaforo4");
	sem_unlink("semaforo5");
	sem_unlink("semaforo6");
	sem_unlink("semaforo7");
	sem_unlink("semaforo8");		
 return 0;
}
