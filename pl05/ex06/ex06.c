#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/time.h>
#include <semaphore.h>
#include <pthread.h>


void * findPrimeNumber(void*);

int main(){
	pthread_t thread;
	int number=0;
	
	while(number<=1){
		printf("Introduza um número superior a 1:\n");
		scanf("%d",&number);
	}

	pthread_create(&thread, NULL, findPrimeNumber, (void*) &number);

	printf("Primos menores que %d:\n", number);

	pthread_join(thread, NULL);
	
	return 0;	
	
}

void* findPrimeNumber(void* number){
	int i = 1, flag, j;
	int* n = (int*) number;

	while(i < *n){
		flag = 0;
		
		for(j = 2; j <= i/2; j++){
			if(i % j == 0){
				flag = 1;
				break;
			}
		}
		
		if(flag == 0){
			printf("%d\n", i);
		}
		i++;
	}
	
	pthread_exit(NULL);
}