#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h> 
#define MAX_PROVAS 300

typedef struct {
	int number; //student number
	int notaG1;
	int notaG2;
	int notaG3;
	int notaFinal;
}Prova;

int indexOfLast;
int pos=0;
int neg=0;
int fim;
Prova arrayProva[MAX_PROVAS];
pthread_mutex_t muxter;
pthread_cond_t cond;
pthread_cond_t condMaior;
pthread_cond_t condMenor;

void* generateProvas(void* arg){
	srand(time(NULL)); 

	for(int i=1; i<=MAX_PROVAS;i++){
		Prova p;
		p.number = 1180000 + i;
		p.notaG1 = rand() % 101;
		p.notaG2 = rand() % 101;
		p.notaG3 = rand() % 101;

		pthread_mutex_lock(&muxter);
		
		printf("\nAdding the Prova: \n  -Number=%d \n  -NotaG1=%d%c \n  -NotaG2=%d%c \n  -NotaG3=%d%c \n",p.number,p.notaG1,'%',p.notaG2,'%',p.notaG3,'%');
		arrayProva[i] = p;
		//por exemplo =0
		indexOfLast = i;
		
		pthread_mutex_unlock(&muxter);
		pthread_cond_signal(&cond);
	}
	pthread_exit(NULL);
}

void* notaFinalCalculation(void* arg){
	int aux;
	do{
		pthread_mutex_lock(&muxter);
		//espera pelo ok de cima			
		pthread_cond_wait(&cond,&muxter);
		//CURRENT PROVA
		aux = indexOfLast;
		Prova p = arrayProva[aux];		
		int calculation = 0;		
		
		calculation = (p.notaG1 + p.notaG2 + p.notaG3) / 3;
		arrayProva[aux].notaFinal = calculation;

		if(calculation>=50){
			pthread_cond_signal(&condMaior);
		}else{
			pthread_cond_signal(&condMenor);
		}

		printf("Nota do índice %d = %d%c \n", aux, calculation,'%');
		pthread_mutex_unlock(&muxter);
		
	}while(aux!=MAX_PROVAS);
	fim=1;
	pthread_exit(NULL);
}

void* notaPositiva(void* arg){
	while(fim!=1){
		pthread_mutex_lock(&muxter);
		pthread_cond_wait(&condMaior,&muxter);
		printf("A nota é maior que 50!\n");
		pos++;	
	pthread_mutex_unlock(&muxter);
	}	
	pthread_exit((void*)NULL);
}

void* notaNegativa(void* arg){
	while(fim!=1){
		pthread_mutex_lock(&muxter);	
		pthread_cond_wait(&condMenor,&muxter);
		printf("A nota é menor que 50!\n");
		neg++;

		pthread_mutex_unlock(&muxter);
	}
	pthread_exit((void*)NULL);
}

int main(){

	pthread_t thread_ids[5];

	pthread_mutex_init(&muxter, NULL);
	pthread_cond_init(&cond,NULL);
	pthread_cond_init(&condMaior,NULL);
	pthread_cond_init(&condMenor,NULL);

	pthread_create(&thread_ids[0], NULL, generateProvas, (void*)NULL);
	pthread_create(&thread_ids[1], NULL, notaFinalCalculation, (void*)NULL);
	pthread_create(&thread_ids[2], NULL, notaFinalCalculation, (void*)NULL);
	pthread_create(&thread_ids[3], NULL, notaPositiva, (void*)NULL);
	pthread_create(&thread_ids[4], NULL, notaNegativa, (void*)NULL);

	pthread_join(thread_ids[0],NULL);
	int index ;
	index = indexOfLast;

	if(index!=300){
		pthread_join(thread_ids[1],NULL);
		pthread_join(thread_ids[2],NULL);
	}
	
	if(fim!=1){
		pthread_join(thread_ids[3],NULL);	
		pthread_join(thread_ids[4],NULL);	
	}
	
	int auxPos,auxNeg;
	auxPos = pos;
	auxNeg = neg;

	float percentagemPos = (float)auxPos/300;
	float percentagemNeg = (float)auxNeg/300;

	printf("\nQtd POS=%.2f\n",percentagemPos);
	printf("\nQtd NEG=%.2f\n",percentagemNeg);

	pthread_cond_destroy(&condMenor);
	pthread_cond_destroy(&condMaior);			
	pthread_cond_destroy(&cond);
	pthread_mutex_destroy(&muxter);
	

	return 0;
}