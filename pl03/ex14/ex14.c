#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <limits.h>


#define STR_SIZE 50
#define NR_DISC 10
#define	SIZE_OF_BUFFER		    8	// Maximum size of buffer

typedef struct cbuffer{
	int circularBuffer[SIZE_OF_BUFFER] = { 0 };	// Empty circular buffer
	int readIndex=0;	// Index of the read pointer
	int writeIndex=0;	// Index of the write pointer
	int bufferLength=0;	// Number of values in circular buffer

} shm_data_type;


int main() {
	int i,fd,r,p;

	shm_data_type *shared_mem;

	/* read,write,execute/search permissions by owner and group */
	fd = shm_open("/shmHSP", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);

	/* Verifica se a memória partilhada foi devidamente aberta */
	if (fd < 0) {
		perror("No smh_open!");
		exit(0);
	}

	/* Ajusta o tamanho da memória partilhada */
	ftruncate(fd, sizeof(shm_data_type));

	/* Mapea a memória partilhada */
	shared_mem = (shm_data_type *) mmap(NULL, sizeof(shm_data_type), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);

	/* Verifica se a memória partilhada foi devidamente mapeada */
	if (shared_mem == NULL) {
		printf("No mmap()");
		exit(0);
	}
	shared_mem->new_data=0;
	p = fork();

		if(p==0){ //Filho
			while(!shared_mem->new_data);
			
			exit(0);

		
		}else{//Pai
			


			shared_mem->new_data=1;
			wait(&a);
		

		}
	

	/* Desfaz o mapeamento */
	r = munmap(shared_mem, sizeof(shared_mem));

	if (r < 0) {
		perror("No munmap()!");
		exit(1);
	}

	return 0;
}




