#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <semaphore.h>

int main() {
	
	int *shm_counter=NULL; /* apontador para a memória partilhada (um inteiro) */
    int shmfd; /* descritor da memória partilhada */
    int i, r;
    pid_t pid;
        
    /* criar o objeto de memoria partilhada */
    shmfd = shm_open("/hsp_shm", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
    if (shmfd < 0) {
        perror("No shm_open()");
        exit(1);
    }
    
    /* ajustar o tamanho da mem. partilhada */
    ftruncate(shmfd, sizeof(int));
    
    /* mapear a mem. partilhada */
    shm_counter = (int *)mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
    if (shm_counter == NULL) {
        perror("No mmap()");
        exit(1);
    }
    
    *shm_counter = 0; /* iniciar a zero (na realidade, o mmap() já o faz) */
	
	/**
	 * Criar o semaforo com o valor 0.
	 */
	sem_t *semaforo;
	if ((semaforo = sem_open("sem_pl4_ex5", O_CREAT | O_CREAT, 0644, 0))== SEM_FAILED) {
		perror("No sem_open()");
		exit(0);
	}


	int p = fork();
	/**
	 * Sendo filho, imprime e adiciona 1 ao semáforo.
	 * O pai espera que o filho termine através do
	 * semáforo.
	 */
	while((*shm_counter) < 15){
		if (p == 0) {
			
			printf("I'm the child.\n");
			(*shm_counter)++;
			//printf("child counter: %d\n", (*shm_counter));
			sem_post(semaforo);
			
			if((*shm_counter) == 15){
				exit(0);
			}
		}
		else{
			sem_wait(semaforo);
			if((*shm_counter) == 15){
				sem_post(semaforo);
			}else{
				printf("I'm the father.\n");
				(*shm_counter)++;
				//printf("father counter: %d\n", (*shm_counter));
			}
		}
	}

	if (sem_close(semaforo) < 0) {
		perror("No unlink()");
		exit(0);
	}

	if (sem_unlink("sem_pl4_ex5") < 0) {
		perror("No unlink()");
		exit(0);
	}
	
	/* desfaz mapeamento */
    r=munmap(shm_counter, sizeof(int));
    if (r < 0) { /* verifica erro */
        perror("No munmap()");
        exit(1);
    }
    
    /* apaga a memoria partilhada do sistema */
    r=shm_unlink("//hsp_shm");
    if (r < 0) { /* verifica erro */
        perror("No unlink()");
        exit(1);
    }
}