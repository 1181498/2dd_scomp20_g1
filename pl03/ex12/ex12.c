#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <limits.h>


#define STR_SIZE 50
#define NR_DISC 10

int get_min(int* vec,int size){
	int i;
	int min=INT_MAX;
	for(i=0;i<size;i++){
		if(vec[i]<min){
			min=vec[i];		
		}
	}	
	return min;
}

int get_max(int* vec,int size){
	int i;
	int max=INT_MIN;
	for(i=0;i<size;i++){
		if(vec[i]>max){
			max=vec[i];		
		}
	}	
	return max;
}

float get_average(int* vec,int size){
	int i;
	float average=0;
	int sum=0;
	for(i=0;i<size;i++){
		sum+=vec[i];	
	}
	average = sum/size;
	return average;
}



typedef struct aluno{
	int new_data;
	int numero;
	char nome[STR_SIZE];
	int disciplinas[NR_DISC];
} shm_data_type;


int main() {
	int i,fd,r,p;

	shm_data_type *shared_mem;

	/* read,write,execute/search permissions by owner and group */
	fd = shm_open("/shmHSP", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);

	/* Verifica se a memória partilhada foi devidamente aberta */
	if (fd < 0) {
		perror("No smh_open!");
		exit(0);
	}

	/* Ajusta o tamanho da memória partilhada */
	ftruncate(fd, sizeof(shm_data_type));

	/* Mapea a memória partilhada */
	shared_mem = (shm_data_type *) mmap(NULL, sizeof(shm_data_type), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);

	/* Verifica se a memória partilhada foi devidamente mapeada */
	if (shared_mem == NULL) {
		printf("No mmap()");
		exit(0);
	}
	shared_mem->new_data=0;
	p = fork();

		if(p==0){ //Filho
			while(!shared_mem->new_data);
			int z,min,max;
			float average;
			min=get_min(shared_mem->disciplinas,NR_DISC);
			max=get_max(shared_mem->disciplinas,NR_DISC);
			average=get_average(shared_mem->disciplinas,NR_DISC);

			printf("Filho\n");
			printf("Numero do aluno: %d\n",shared_mem->numero);
			printf("Nome do aluno: %s\n",shared_mem->nome);
			printf("Disciplinas:\n");
			for(z=0;z<NR_DISC;z++){
				printf("%d;",shared_mem->disciplinas[z]);
			}
			printf("\nHighest Grade:%d\n",max);
			printf("Lowest Grade:%d\n",min);
			printf("Average Grade:%f\n",average);
			exit(0);

		
		}else{//Pai
			int temp_num,s,a;
			char temp_name[STR_SIZE];
			int temp_disc[NR_DISC];
			printf("Numero do aluno:\n");
			scanf("%d",&temp_num);
			printf("Nome do aluno:\n");
			scanf("%s",temp_name);
			printf("Disciplinas:\n");
			for(s=0;s<NR_DISC;s++){
				printf("%d ª disciplina:\n",s+1);
				scanf("%d",&temp_disc[s]);
			}
			shared_mem->numero=temp_num;
			strcpy(shared_mem->nome,temp_name);
			for(s=0;s<NR_DISC;s++){
				shared_mem->disciplinas[s]=temp_disc[s];
			}
			shared_mem->new_data=1;
			wait(&a);
		

		}
	

	/* Desfaz o mapeamento */
	r = munmap(shared_mem, sizeof(shared_mem));

	if (r < 0) {
		perror("No munmap()!");
		exit(1);
	}

	return 0;
}




