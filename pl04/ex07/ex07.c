#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/time.h>
#include <semaphore.h>


int main(){
	pid_t p,p2;
	sem_t *sem,*sem2,*sem3;

	if ((sem = sem_open("/sema", O_CREAT | O_EXCL, 0600, 0)) == SEM_FAILED) {
		printf("Semáforo já existe\n");
		return -1;
	}
	
	if ((sem2 = sem_open("/sema2", O_CREAT | O_EXCL, 0600, 0)) == SEM_FAILED) {
		printf("Semáforo já existe\n");
		return -1;
	}
	
	if ((sem3 = sem_open("/sema3", O_CREAT | O_EXCL, 0600, 0)) == SEM_FAILED) {
		printf("Semáforo já existe\n");
		return -1;
	}
	
	printf("Sistemas ");
	fflush(stdout);
	
	p=fork();
	
	if (p>0)
	{
		
		p2=fork();
		
		if (p2>0)
		{
			sem_wait(sem2);
			printf("a ");
			fflush(stdout);
			sem_post(sem3);
		}
		else{
			sem_wait(sem);
			printf("Computadores -");
			fflush(stdout);
			sem_post(sem2);
			
			sem_wait(sem);
			printf("disciplina\n");
			
		}
		
	}
	else{
		
		printf("de ");
		fflush(stdout);
		sem_post(sem);
		
		sem_wait(sem3);
		printf("melhor ");
		fflush(stdout);
		sem_post(sem3);
		sem_post(sem);
		
	}
	
	sem_unlink("/sema");
	sem_unlink("/sema2");
	sem_unlink("/sema3");
	return 0;	
	
}
