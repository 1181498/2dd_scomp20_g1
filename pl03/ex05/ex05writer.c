#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>

#define ARRAY_SIZE 100
#define MINIMUM 65
#define MAXIMUM 90

/* WRITER */

typedef struct {
	int a;
	int b;
} shm_data_type;

int main() {
	int fd, i, r,p;
	time_t t;
	shm_data_type *shared_mem;

	/* read,write,execute/search permissions by owner and group */
	fd = shm_open("/shmHSP", O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);

	/* Verifica se a memória partilhada foi devidamente aberta */
	if (fd < 0) {
		perror("No smh_open!");
		exit(0);
	}

	/* Ajusta o tamanho da memória partilhada */
	ftruncate(fd, sizeof(shm_data_type));

	/* Mapea a memória partilhada */
	shared_mem = (shm_data_type *) mmap(NULL, sizeof(shm_data_type), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);

	/* Verifica se a memória partilhada foi devidamente mapeada */
	if (shared_mem == NULL) {
		printf("No mmap()");
		exit(0);
	}


	shared_mem->a=8000;
	shared_mem->b=200;
	
	int num_times=1000000;	

	p = fork();

	
	for(i=0;i<num_times;i++){		
		if(p==0){ //Filho
			shared_mem->a-=1;
			shared_mem->b+=1;
	printf("shared_mem a : %d shared_mem b : %d\n",shared_mem->a,shared_mem->b);		
		
		}else{//Pai
			shared_mem->a+=1;
			shared_mem->b-=1;	
	printf("shared_mem a : %d shared_mem b : %d\n",shared_mem->a,shared_mem->b);	
		}
	}
	

	if(p==0){ //Filho
		exit(0);	
		}

	printf("shared_mem a : %d shared_mem b : %d\n",shared_mem->a,shared_mem->b);

	/* Desfaz o mapeamento */
	r = munmap(shared_mem, sizeof(shared_mem));

	if (r < 0) {
		perror("No munmap()!");
		exit(1);
	}

	return 0;
}

//Não, o acesso concorrente ao mesmo recurso(espaço de memória) cria um estado inconsistente.


